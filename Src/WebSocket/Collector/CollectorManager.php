<?php

/**
 * This file is part of Rangine
 *
 * (c) We7Team 2019 <https://www.rangine.com/>
 *
 * document http://s.w7.cc/index.php?c=wiki&do=view&id=317&list=2284
 *
 * visited https://www.rangine.com/ for more details
 */

namespace W7\WebSocket\Collector;

use W7\Tcp\Collector\CollectorManager as CollectManagerAbstract;

/**
 * 统一收集连接时的资源
 * 统一释放断开后的无用资源
 * Class CollectorManager
 * @package W7\WebSocket\Collector
 */
class CollectorManager extends CollectManagerAbstract {

}
